import React from 'react';
import './Navigation.css';
import {NavLink} from 'react-router-dom';

const Navigation = () => {
    return (
        <div className="navigation">
            <span className="logo">Static Pages</span>
            <ul>
                <li><NavLink to="/" exact>Home</NavLink></li>
                <li><NavLink to="/about">About</NavLink></li>
                <li><NavLink to="/contacts">Contacts</NavLink></li>
                <li><NavLink to="/divisions">Divisions</NavLink></li>
                <li><NavLink to="/services">Services</NavLink></li>
                <li><NavLink to="/pages/admin">Admin</NavLink></li>
            </ul>
        </div>
    )
};

export default Navigation;