import React from 'react';
import './Content.css';

const Content = props => {
    return (
        <div className="content-container">
            <h3>{props.title}</h3>
            <div className="content">{props.content}</div>
        </div>
    )
};

export default Content;