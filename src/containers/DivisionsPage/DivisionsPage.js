import React, {Component} from 'react';
import Content from '../../components/Content/Content';
import axios from "axios/index";

class DivisionsPage extends Component {
    state = {
        info: {},
        loading: true
    };

    componentDidMount() {
        axios.get('/divisions.json').then(response => {
            this.setState({info: response.data, loading: false});
        });
    }

    render() {
        if (this.state.loading) {
            return (
                <h2 className="loading" />
            )
        } else {
            return (
                <Content title={this.state.info.title} content={this.state.info.content} />
            )
        }
    }
}

export default DivisionsPage;